package com.veriflix.sample.android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.veriflix.android.client.VeriFlixSdk
import com.veriflix.android.client.service.AuthenticationService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        status_text.text = getString(if (VeriFlixSdk.isInitialized()) {
            R.string.sdk_is_inited
        } else {
            R.string.sdk_is_not_inited
        })
        login.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra(LoginActivity.SIGN_UP, false)
            startActivity(intent)
        }

        register.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra(LoginActivity.SIGN_UP, true)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if (VeriFlixSdk.isInitialized() &&
            AuthenticationService.instance.isAuthenticated()) {
            val intent = Intent(this, Main2Activity::class.java)
            startActivity(intent)
        }
    }
}
