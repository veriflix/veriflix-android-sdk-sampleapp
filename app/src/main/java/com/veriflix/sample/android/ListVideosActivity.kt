package com.veriflix.sample.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.veriflix.android.client.service.RecordService
import kotlinx.android.synthetic.main.activity_list_videos.*

class ListVideosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_videos)
        refresh.setOnClickListener {
            logs.text = "Refreshing"
            RecordService.instance.requestVideos(null, {
                val log = StringBuilder()
                log.append("count = ")
                    .append(it.count)
                    .append('\n')
                    .append("next page link = ")
                    .append(it.next)
                    .append('\n')
                    .append('\n')
                    .append("Results:")
                it.results.forEach {
                    log.append('\n')
                        .append('\n')
                        .append(it.toString())
                }
                logs.text = log
            }, {
                logs.text = it?.message ?: "Failure"
            })
        }
    }
}
