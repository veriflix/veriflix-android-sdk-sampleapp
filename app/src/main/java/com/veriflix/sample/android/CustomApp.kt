package com.veriflix.sample.android

import android.app.Application
import com.veriflix.android.client.VeriFlixSdk
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class CustomApp : Application() {
    lateinit var veriFlixSdk: VeriFlixSdk
        internal set

    override fun onCreate() {
        super.onCreate()
        val executor = Executors.newFixedThreadPool(1) as Executor //example
        veriFlixSdk = VeriFlixSdk.Builder()
            .applicationContext(this)
            .networkExecutor(executor)
//            .consumerId("paste_your_consumer_id_here")
            .build()

    }
}