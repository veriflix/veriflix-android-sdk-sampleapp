package com.veriflix.sample.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.veriflix.android.client.service.DeviceService
import kotlinx.android.synthetic.main.send_push_token_activity_layout.*

class SendPushTokenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_push_token_activity_layout)
        val log = StringBuffer()
        log.append("Sending logs:")
        sending_logs.text = log
        send_push_token.setOnClickListener {
            DeviceService.instance.updatePushToken(push_token_input.text.toString(), {
                log.append('\n')
                    .append("Success")
                sending_logs.text = log
            }, {
                log.append('\n')
                    .append(it?.message ?: "Failure")
                sending_logs.text = log
            })
        }
    }
}
