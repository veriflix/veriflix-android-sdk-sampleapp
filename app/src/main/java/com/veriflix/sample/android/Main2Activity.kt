package com.veriflix.sample.android

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.veriflix.android.client.service.*
import com.veriflix.android.ui.RecorderActivity
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        user_name.text = ProfileService.instance.cachedUserProfile.email
        logout.setOnClickListener {
            AuthenticationService.instance.logout(successCallback = {
                Toast.makeText(this@Main2Activity, "success logged out", Toast.LENGTH_SHORT).show()
                finish()
            }, failureCallback = {
                Toast.makeText(this@Main2Activity, "failure $it", Toast.LENGTH_LONG).show()
                finish()
            })
        }
        record_self_video.setOnClickListener {
            RecorderActivity.openRecorder(this@Main2Activity)
        }
        show_send_location.setOnClickListener {
            val intent = Intent(this, SendLocationActivity::class.java)
            startActivity(intent)
        }
        show_recorded_videos.setOnClickListener {
            val intent = Intent(this, ListVideosActivity::class.java)
            startActivity(intent)
        }
        show_near_missions.setOnClickListener {
            val intent = Intent(this, MissionsActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if (AuthenticationService.instance.isAuthenticated()) {
            SettingsService.instance.requestMediaSettings({
                ProfileService.instance.requestUserProfile({
                    user_name.text = it.email
                    GamificationService.instance.requestGameMilestones({}, {})
                }, {})
            }, {})
        } else {
            finish()
        }
    }

    override fun onBackPressed() {
        if (AuthenticationService.instance.isAuthenticated()) {
            finishAffinity()
        } else {
            super.onBackPressed()
        }
    }
}
