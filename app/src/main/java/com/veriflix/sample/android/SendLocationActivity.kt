package com.veriflix.sample.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.veriflix.android.client.service.SettingsService
import kotlinx.android.synthetic.main.activity_send_location.*

class SendLocationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_location)
        send_location.setOnClickListener {
            lat_input_layout.error = null
            lng_input_layout.error = null
            val logBuilder = StringBuffer()
            logBuilder.append("Sending logs:").append('\n')
            sending_logs.text = logBuilder
            val latitudeText = latitude_input.text.toString()
            val longitudeText = longitude_input.text.toString()
            var latValue: Double = 0.0
            var lngValue: Double = 0.0
            if (latitudeText.isEmpty()) {
                lat_input_layout.error = "Error: empty"
            } else {
                latValue = latitudeText.toDoubleOrNull() ?: kotlin.run {
                    lat_input_layout.error = "Error: invalid"
                    0.0
                }
            }
            if (longitudeText.isEmpty()) {
                lng_input_layout.error = "Error: empty"
            } else {
                lngValue = longitudeText.toDoubleOrNull() ?: kotlin.run {
                    lng_input_layout.error = "Error: invalid"
                    0.0
                }
            }
            if (lat_input_layout.error == null && lng_input_layout.error == null) {
                SettingsService.instance.updateCurrentPosition(lngValue, latValue, {
                    logBuilder.append('\n')
                        .append("success")
                    sending_logs.text = logBuilder
                }, {
                    logBuilder.append('\n')
                        .append(it?.message ?: "Failed")
                    sending_logs.text = logBuilder
                })
            }
        }
    }
}
