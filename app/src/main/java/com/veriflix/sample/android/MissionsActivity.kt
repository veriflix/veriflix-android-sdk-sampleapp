package com.veriflix.sample.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.veriflix.android.client.service.MissionService
import kotlinx.android.synthetic.main.activity_missions.*

class MissionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_missions)
        refresh.setOnClickListener {
            logs.text = "Refreshing"
            MissionService.instance.requestNearestMissions(false, {
                val log = StringBuilder()
                log.append("count = ")
                    .append(it.count)
                    .append('\n')
                    .append("next page link = ")
                    .append(it.next)
                    .append('\n')
                    .append('\n')
                    .append("Results:")
                it.results.forEach {
                    log.append('\n')
                        .append('\n')
                        .append(it.toString())
                }
                logs.text = log
            }, {
                logs.text = it?.message ?: "Failure"
            })
        }
    }
}
